<?php
namespace Controllers;

use \Services\ApiService as ApiService;

class ApiController {
    private $apiService;

    public function __construct(ApiService $apiService){
        $this->apiService = $apiService;
    }

    public function onGet(array $pathArray = []){
        if(is_null($this->apiService->onGet($pathArray)) ) {
            http_response_code(404);
            header('Content-type: application/json');
            return json_encode(array('Error' => 'Cannot find nothing on this path'));
            //return $this->apiService->onGet($pathArray);
        } 
       
        http_response_code(200);
        header('Content-type: application/json');
        return $this->apiService->onGet($pathArray);

    }

    public function onPost(array $pathArray = [], $requestBody = null){
        if(is_null($this->apiService->onPost($pathArray, $requestBody)) ) {
            http_response_code(400);
            header('Content-type: application/json');
            return json_encode(array('Error' => 'Create failed'));
        } 

        http_response_code(200);
        header('Content-type: application/json');
        return json_encode(array('Message' => 'Creation was successful.'));
    }

    public function onPut(array $pathArray = [], $requestBody = null){
        if(is_null($this->apiService->onPut($pathArray, $requestBody)) ) {
            http_response_code(400);
            header('Content-type: application/json');
            return json_encode(array('Error' => 'Update failed because the path was unknown'));
        } 

        http_response_code(200);
        header('Content-type: application/json');
        return json_encode(array('Message' => 'Update was successful.'));//'Successful';
    }

    public function onDelete(array $pathArray = []){
        $pathString = implode('.', $pathArray);

        if(is_null($this->apiService->onDelete($pathArray)) ) {
            http_response_code(400);
            header('Content-type: application/json');
            return json_encode(array('Error' => 'Delete failed because the path was unknown'));
        } 

        http_response_code(200);
        header('Content-type: application/json');
        return json_encode(array('Message' => 'Delete was successful.'));//'Successful';
    }
}