<?php
namespace Services;

use \PDO as PDO;

class PdoService {
    private $pdo;

    public function __construct(){
        $config = \parse_ini_file('public/db.ini');
        try {
            $this->pdo = new PDO($config['dsn'], $config['user'], $config['pass']);
        }
        catch(\PDOException $exception){
            echo 'Error: ' . $exception->getMessage();  
        }
    }

    public function getAccessLevelForResourceByMongodbIdAndApiKey(string $mongodbId, string $apiKey){
        $stmt = $this->pdo->prepare("SELECT ur.user_id, ur.resource_id, r.mongodb_id, al.id AS 'access_level_id', al.name as 'access_level_name'
                                    FROM users_resources AS ur
                                    INNER JOIN resources AS r
                                    ON ur.resource_id = r.id
                                    INNER JOIN access_level AS al
                                    ON ur.access_level_id = al.id
                                    WHERE r.mongodb_id = :mongodbId AND ur.api_key = :apiKey;");
        $stmt->bindParam(':mongodbId', $mongodbId);
        $stmt->bindParam(':apiKey', $apiKey);

        // var_dump($stmt->execute());exit;
        
        return ($stmt->execute()) ? $stmt->fetch(PDO::FETCH_ASSOC)['access_level_name']: array();
        
    }

    public function __desctuct(){
        $this->pdo = null;
    }
}