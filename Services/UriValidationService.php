<?php
namespace Services;

class UriValidationService {

    protected $uri;
    protected $uriArray;

    public static function test(){
        return 'Hello from UriService';
    }

    public function __construct($uri) {
        $this->uri = $uri;
        $this->uriArray = $this->toArray();
    }

    public function checkUriForApi() : bool {
        return ($this->uriArray[1] === 'api') ? true : false;
    }

    public function checkPathString() : bool {
        $pathString = $this->getPathStringFromUri();

        // var_dump($pathString);

        //[<>?;\'\\:"|[\]{}!@#$%^&*()_\+\-\=*\ а-яА-Я]+
        if (preg_match('/^[a-zA-Z0-9\/\_]+$/', $pathString) === 1 or empty($pathString)) {
            return true;
        } 

        return false;
    }

    public function getResourceId() : string {

        if($this->checkUriForApi() === false){
            http_response_code(400);
            echo json_encode(array('Error' => 'Incorrect Uri. \'api\' is missing.'));
            return;
        }

        return $this->uriArray[2];
    }

    public function checkResourceId($mongoCollection) : bool {
        try {
            $result = $mongoCollection->findOne(['_id' =>  new \MongoDB\BSON\ObjectId($this->getResourceId())]);
        } catch (\MongoDB\Driver\Exception\InvalidArgumentException $e) { 
            $result = $mongoCollection->findOne(['_id' =>  $this->getResourceId()]);
        } catch(\Exception $e) {
            $result = null;
        }

        return (is_null($result) === true) ? false : true;
    }

    public function getPathFromUri() : array{

        if($this->checkUriForApi() === false){
            http_response_code(400);
            echo json_encode(array('Error' => 'Incorrect Uri. \'api\' is missing.'));
            return;
        }

        $pathArray = array();

        for ($i = 3; $i < count($this->uriArray); $i++) { 
            $pathArray[] = $this->uriArray[$i];
        }

        $pathArray = array_filter(
            $pathArray,
            function(string $element) {
                return $element !== '';
            }
        );

        return $pathArray;
    }

    public function getPathStringFromUri() : string {
        return implode('/', $this->getPathFromUri());
    }

    protected function toArray() : array {
        return explode('/', $this->uri);
    }
}