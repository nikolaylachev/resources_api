<?php

namespace Services;

class ApiService {
    private $mongoConnection;
    private $mongoCollection;
    private $resourceId;

    public function __construct($mongoConnection, $mongoCollection, $resourceId){
        $this->mongoConnection = $mongoConnection;
        $this->mongoCollection = $mongoCollection;
        $this->resourceId = $resourceId;
    }

    public function onGet(array $pathArray = []){
        try {
            $result = $this->mongoCollection->findOne(['_id' =>  new \MongoDB\BSON\ObjectId($this->resourceId)]);
        } catch (\Exception $e) { 
            $result = $this->mongoCollection->findOne(['_id' => $this->resourceId]);
        }

        if (empty($pathArray[0]) ) {
            return json_encode($result);
        }
        
        foreach ($pathArray as $value) {
            if(is_null($result) ) {
                break;
            }
        
            $result = $result[$value];
        }

        if(is_null($result) ) {
            return null;//json_encode($result);
        } 
       
        return json_encode($result);

    }

    public function onPost(array $pathArray = [], $requestBody = null){
        
        if (empty($pathArray[0]) ) {
            return json_encode(array('Error' => 'No path added.')); //json_encode($result);
        }

        if(is_null(json_decode($this->onGet($pathArray))) ) {
            return $this->onPut($pathArray, $requestBody);
        }

        if(is_array(json_decode($this->onGet($pathArray))) === false) {
            $firstElementArray = json_decode($this->onGet($pathArray));

            $this->onDelete($pathArray);
            $this->onPut($pathArray, json_decode('[]'));

            $pathArray[] = '0';
            $this->onPut($pathArray, $firstElementArray);
    
            $pathArray[count($pathArray) - 1] = '1';
            return $this->onPut($pathArray, $requestBody);
            //return;
        }

        $nextElementIndex = count(json_decode($this->onGet($pathArray)));
        $pathArray[] = $nextElementIndex;

        return $this->onPut($pathArray, $requestBody);
    }

    public function onPut(array $pathArray = [], $requestBody = null){
        $pathString = implode('.', $pathArray);
        
        //db.users.update ({_id: '123'}, { '$set': {"friends.0.emails.0.email" : '2222'} });
        try {
            $result = $this->mongoCollection->updateOne(
                ['_id' =>  new \MongoDB\BSON\ObjectId($this->resourceId)],
                ['$set' => [$pathString => $requestBody]]
            );
        } catch(\MongoDB\Driver\Exception\InvalidArgumentException $e){
            return null;
        } catch(\Exception $e) {
            $result = $this->mongoCollection->updateOne(
                ['_id' =>  $this->resourceId],
                ['$set' => [$pathString => $requestBody]]
            );
        }

        //http_response_code(200);
        return 'Successful';
    }

    public function onDelete(array $pathArray = []){
        $pathString = implode('.', $pathArray);

        if(is_null(json_decode($this->onGet($pathArray))) ) {
            return null;
        } 

        //db.users.update ({_id: '123'}, { '$unset': {"friends.0.emails.0.email" : ''} });
        try {
            $result = $this->mongoCollection->updateOne(
                ['_id' =>  new \MongoDB\BSON\ObjectId($this->resourceId)],
                ['$unset' => [$pathString => '']]
            );
        } catch(\MongoDB\Driver\Exception\InvalidArgumentException $e){
            return null;
        }catch(\Exception $e) {
            $result = $this->mongoCollection->updateOne(
                ['_id' =>  $this->resourceId],
                ['$unset' => [$pathString => '']]
            );
        }

        //http_response_code(200);
        return 'Successful';
    }
}