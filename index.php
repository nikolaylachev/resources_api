<?php
require_once 'common.php';

use \Services\UriValidationService;
use \Controllers\ApiController;
use \Services\ApiService as ApiService;
use \Services\PdoService as PdoService;

if (isset($requestHeaders['Api-Key']) === false) {
    header('Content-type: application/json');
    echo json_encode(array('Error' => 'Please enter your api key as a request header with key: \'Api-Key\' and value your api key'));
    return;
}

$uriService = new UriValidationService($_SERVER['REQUEST_URI']);

if($uriService->checkUriForApi() === false) {
    http_response_code(400);
    header('Content-type: application/json');
    echo json_encode(array('Error' => 'Incorrect Uri. \'api\' is missing or is incorrectly spelled.'));
    return;
}

if($uriService->checkResourceId($collection) === false) {
    http_response_code(400);
    header('Content-type: application/json');
    echo json_encode(array('Error' => 'Incorrect resource id.'));
    return;
}

if($uriService->checkPathString() === false) {
    http_response_code(400);
    header('Content-type: application/json');
    echo json_encode(array('Error' => 'Incorrect Uri. Path contains illegal characters.'));
    return;
}

$apiService = new ApiService($mongoConnection, $collection, $uriService->getResourceId());
$apiController = new ApiController($apiService);

$pdoService = new PdoService();
$accessLevelForResource = $pdoService
                        ->getAccessLevelForResourceByMongodbIdAndApiKey(
                            $uriService->getResourceId(), $requestHeaders['Api-Key']
                        );

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        // $pathArray = ($uriService->getPathFromUri() === null) ? array() : $uriService->getPathFromUri();
        if($accessLevelForResource === 'Read' or $accessLevelForResource === 'Read, Create, Update' 
            or $accessLevelForResource === 'Read, Create, Update, Delete'){
            echo $apiController->onGet($uriService->getPathFromUri());
        } else {
            header('Content-type: application/json');
            echo json_encode(array('Error' => 'You don\'t have the right access level for this resource'));
        }    
        break;

    case 'POST':
        if($accessLevelForResource === 'Read, Create, Update' or $accessLevelForResource === 'Read, Create, Update, Delete'){
            $requestBody = json_decode(file_get_contents("php://input"), true);
            echo $apiController->onPost($uriService->getPathFromUri(), $requestBody);
        } else {
            header('Content-type: application/json');
            echo json_encode(array('Error' => 'You don\'t have the right access level for this resource'));
        }
        break;

    case 'PUT':
        if($accessLevelForResource === 'Read, Create, Update' or $accessLevelForResource === 'Read, Create, Update, Delete') {
            $requestBody = json_decode(file_get_contents("php://input"), true);
            echo $apiController->onPut($uriService->getPathFromUri(), $requestBody);
        } else {
            header('Content-type: application/json');
            echo json_encode(array('Error' => 'You don\'t have the right access level for this resource'));
        }
        break;

    case 'DELETE':
        if($accessLevelForResource === 'Read, Create, Update, Delete') {
            echo $apiController->onDelete($uriService->getPathFromUri());
        } else {
            header('Content-type: application/json');
            echo json_encode(array('Error' => 'You don\'t have the right access level for this resource'));
        }
        break;
        
    default:
        echo json_encode(array('Error' => 'Request method can only be GET, POST, PUT or DELETE'));
        break;
}
