Resources API
=============

This project is part of my diploma thesis for my bachelor degree at the University Of National And World Economy.

It is created in addition to [Resources Sharing Platform](https://bitbucket.org/nikolaylachev/resources-sharing-platform/src/master/) project. It's only goal is the creation of an API that users can use to easily retrieve, add, edit, and remove data from their own resources or the ones shared with them.