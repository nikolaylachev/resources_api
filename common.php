<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once 'Controllers/ApiController.php';
require_once 'Services/UriValidationService.php';
require_once 'Services/RequestHeadersService.php';
require_once 'Services/ApiService.php';
require_once 'Services/PdoService.php';


use \Services\RequestHeadersService;

$mongoConnection = new MongoDB\Client('mongodb://localhost:27017');
$collection = $mongoConnection->php_api->resources;

if (!function_exists('getallheaders')) {
    $requestHeaders = RequestHeadersService::getAllHeaders();
}